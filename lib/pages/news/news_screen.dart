import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({Key? key}) : super(key: key);

  @override
  State<NewsScreen> createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  var temp = "";

  Future getJsonData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? userToken = await prefs.getString("user-token");

    try {
      var url = Uri.https('demoapi-hilmy.sanbercloud.com', '/api/news-all');
      final response = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $userToken',
      });
      var jsonData = jsonDecode(response.body);
      if (response.statusCode == 200) {
        print(jsonData['data']);
        return jsonData['data'];
      } else {
        print('Gagal');
      }
    } catch (e) {
      print("error");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<dynamic>(
        future: getJsonData(),
        builder: (context, snapshot) {
          if (snapshot.error != null) {
            return Text(
              "${snapshot.error}",
              style: const TextStyle(fontSize: 20),
            );
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else {
            return Padding(
              padding: EdgeInsets.all(30),
              child: Column(
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(left: 15),
                      child: Text(
                        "News",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 30),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Image.asset("assets/images/work1.png"),
                    Container(
                      margin: EdgeInsets.only(top: 10, left: 15),
                      alignment: Alignment.centerLeft,
                      child: Text("Selasa, 11 Oktober 2022"),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 15),
                      // alignment: Alignment.centerLeft,
                      child: Text(
                        "Judul Berita",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10, left: 15),
                      // alignment: Alignment.centerLeft,
                      child: Text(
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 15),
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Berita lainnya",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Expanded(
                      child: ListView.builder(
                        physics: const AlwaysScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 160,
                                  width: 140,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      // border: Border.all(color: Colors.black),
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 0,
                                          blurRadius: 5,
                                          offset: Offset(5,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          child: Image.asset(
                                            "assets/images/work11.jpg",
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(10),
                                          child: Column(
                                            children: [
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  "${snapshot.data[index]['title']}",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                              // SizedBox(height: 10,),
                                              // Container(
                                              //   alignment: Alignment.centerLeft,
                                              //   child: Text(
                                              //     "Flutter Developer",
                                              //     style: TextStyle(
                                              //         color: Colors.black),
                                              //   ),
                                              // ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    snapshot.data[index]
                                                        ["author"],
                                                    style: TextStyle(
                                                        color: Colors.black
                                                            .withOpacity(0.5)),
                                                  ),
                                                  Text(
                                                    "1 day ago",
                                                    style: TextStyle(
                                                        color: Colors.black
                                                            .withOpacity(0.5)),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
            );
          }
        },
      ),
    );
  }
}
