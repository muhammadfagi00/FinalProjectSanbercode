import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config/dio_http.dart';
import '../login_screen/login_screen.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({Key? key}) : super(key: key);

  @override
  State<AboutScreen> createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  var temp = "";

  void iniState() {
    super.initState();
  }

  Future<dynamic> _logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? userToken = prefs.getString("user-token");
    setState(() {
      temp == userToken;
    });
    try {
      print(":Logout");
      var response = await DioHttp.request.post("/api/logout",
          options: Options(headers: {"authorization": "Bearer $userToken"}));
      SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.clear();
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const LoginScreen()),
      );
    } catch (e) {
      print("gagal");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(30),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage("assets/images/asdasd.jpeg"),
                      radius: 50,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Muhammad Fagi",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        Text("muhammadfagi@gmail.com",
                            style: TextStyle(
                                fontSize: 17,
                                color: Colors.black.withOpacity(0.5)))
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        Text("10",
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.w500)),
                        Text("Lamaran",
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.w500)),
                      ],
                    ),
                    Container(
                      height: 50,
                      width: 1,
                      color: Colors.black,
                    ),
                    Column(
                      children: [
                        Text("10",
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.w500)),
                        Text("Undangan",
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.w500)),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 1,
                  width: 700,
                  color: Colors.black,
                ),
                Container(
                  margin: EdgeInsets.only(top: 35),
                  alignment: Alignment.centerLeft,
                  child: Text("Skill",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.asset(
                      "assets/images/flutter.png",
                      height: 60,
                      width: 60,
                    ),
                    Image.asset(
                      "assets/images/reactjs.png",
                      height: 60,
                      width: 60,
                    ),
                    Image.asset(
                      "assets/images/node.png",
                      height: 60,
                      width: 60,
                    ),
                    Image.asset(
                      "assets/images/laravel.png",
                      height: 60,
                      width: 60,
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.asset(
                      "assets/images/mysql.png",
                      height: 60,
                      width: 60,
                    ),
                    Image.asset(
                      "assets/images/php.png",
                      height: 60,
                      width: 60,
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 1,
                  width: 700,
                  color: Colors.black,
                ),
                Container(
                  margin: EdgeInsets.only(top: 30),
                  alignment: Alignment.centerLeft,
                  child: Text("Sosial Media",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.asset(
                      "assets/images/facebook.png",
                      height: 60,
                      width: 60,
                    ),
                    Image.asset(
                      "assets/images/twitter.png",
                      height: 60,
                      width: 60,
                    ),
                    Image.asset(
                      "assets/images/instagram.png",
                      height: 60,
                      width: 60,
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 60,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    child: TextButton(
                      child: Text(
                        "Logout",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        _logout();
                      },
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
