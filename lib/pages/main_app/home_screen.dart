import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../config/dio_http.dart';
import '../login_screen/login_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController search = new TextEditingController();

  List<String> perusaan = ["Perusahaan A", "Perusahaan B", "Perusahaan C"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(30),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Image.asset(
                      "assets/images/logo.jpg",
                      height: 30,
                      width: 30,
                    ),
                    SizedBox(
                      width: 270,
                      height: 40,
                      child: TextField(
                        controller: search,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: false,
                          hintText: "Cari Lowongan",
                          hintStyle: TextStyle(fontSize: 12),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Cari Kerja",
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                    Icon(
                      Icons.circle_notifications,
                      color: Color(0xff6461E8),
                      size: 35,
                    )
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 5),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Sekarang Juga!",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: SizedBox(
                      height: 150,
                      // width: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                height: 70,
                                width: 64,
                                child: Container(
                                    child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                      "assets/images/all.png",
                                      height: 50,
                                      width: 50,
                                    ),
                                    Text("Semua"),
                                  ],
                                )),
                              ),
                              SizedBox(
                                height: 70,
                                width: 64,
                                child: Container(
                                    child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                      "assets/images/programming.png",
                                      height: 50,
                                      width: 50,
                                    ),
                                    Text("Website"),
                                  ],
                                )),
                              ),
                              SizedBox(
                                height: 70,
                                width: 64,
                                child: Container(
                                    child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                      "assets/images/apps.png",
                                      height: 50,
                                      width: 50,
                                    ),
                                    Text("Mobile"),
                                  ],
                                )),
                              ),
                              SizedBox(
                                height: 70,
                                width: 64,
                                child: Container(
                                    child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                      "assets/images/ux.png",
                                      height: 50,
                                      width: 50,
                                    ),
                                    Text("UI/UX"),
                                  ],
                                )),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                height: 70,
                                width: 64,
                                child: Container(
                                    child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                      "assets/images/internet-security.png",
                                      height: 50,
                                      width: 50,
                                    ),
                                    Text("Security"),
                                  ],
                                )),
                              ),
                              SizedBox(
                                height: 70,
                                width: 64,
                                child: Container(
                                    child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                      "assets/images/megaphone.png",
                                      height: 50,
                                      width: 50,
                                    ),
                                    Text("Marketing"),
                                  ],
                                )),
                              ),
                              SizedBox(
                                height: 70,
                                width: 64,
                                child: Container(
                                    child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                      "assets/images/data-analytics.png",
                                      height: 50,
                                      width: 50,
                                    ),
                                    Text("Database"),
                                  ],
                                )),
                              ),
                              SizedBox(
                                height: 70,
                                width: 64,
                                child: Container(
                                    child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                      "assets/images/more.png",
                                      height: 50,
                                      width: 50,
                                    ),
                                    Text("Lainnya"),
                                  ],
                                )),
                              ),
                            ],
                          ),
                        ],
                      )),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  alignment: Alignment.centerLeft,
                  child: Text("Lamaranmu",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: SizedBox(
                    height: 125,
                    // width: 100,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => buildCategories(index),
                      itemCount: perusaan.length,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      // ),
    );
  }

  Widget buildCategories(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 125,
            width: 140,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                // border: Border.all(color: Colors.black),
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 0,
                    blurRadius: 5,
                    offset: Offset(5, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Image.asset(
                        "assets/images/logo.png",
                        height: 15,
                        width: 15,
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        perusaan[index],
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Flutter Developer",
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Full-time",
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.5)),
                        ),
                        Text(
                          "1 day ago",
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.5)),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
