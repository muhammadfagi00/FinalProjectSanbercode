import 'package:final_project/pages/about/about_screen.dart';
import 'package:final_project/pages/news/news_screen.dart';
import 'package:flutter/material.dart';
import 'home_screen.dart';
// import 'package:rest_api_example/pages/main_app/home_screen.dart';

class MainApp extends StatefulWidget {

  MainApp({Key? key,}) : super(key: key);

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {

  late String? emailSend = "";
  int _selectedIndex = 0;


  static TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  // ignore: prefer_final_fields
  List _widgetOptions = [
    HomeScreen(),
    NewsScreen(),
    AboutScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'News',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Account',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xff6461E8),
        onTap: _onItemTapped,
      ),
    );
  }
}
